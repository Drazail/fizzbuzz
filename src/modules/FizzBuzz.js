

/**
 * defaaultRules
 */

let defaultRules = {
	3: 'Fizz',
	5:'Buzz'
};


/**
 * 
 */
export default class FizzBuzz {

	/**
     * @param {object} rules - optional game rules object, schema : { number : string , ... } 
     */
	constructor(rules = defaultRules){

		// sorting the RulesObject keys
		this.rules = Object.keys(rules).sort();
        
		// constructing the _insert method
		this._insert = (index)=> {
			let insertion = '';
			this.rules.forEach(element=> index%element ===0? insertion = insertion+rules[element]:null);
			insertion === '' ? insertion = index : null;
			return insertion;
		} ;
        
		this.array = null;
	}
    
	/**
     * generates and array based on the current rules
     * @param {int} firstInt 
     * @param {int} lastInt 
     */
	generateArray (firstInt = 1, lastInt = 100){
		this.array = Array.from({length:lastInt- firstInt +1},(value,index)=>{return(this._insert(index+1));});
	}
    
  
	/**
     * changes the rules on an existing instance of the class 
     * @param {object} rules - game rules object, schema : { number : string , ... } 
     */
	changeRules (rules){
		this.rules = Object.keys(rules).sort();
		this._insert = (index)=> {
			let insertion = '';
			this.rules.forEach(element=> index%element ===0? insertion = insertion+rules[element]:null);
			insertion === '' ? insertion = index : null;
			return insertion;
		} ;
	}
}