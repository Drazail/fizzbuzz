'use strict';

var _FizzBuzz = require('./modules/FizzBuzz');

var _FizzBuzz2 = _interopRequireDefault(_FizzBuzz);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var game = new _FizzBuzz2.default();
game.generateArray();
var firstArray = game.array;

process.stdout.write('Array formed by default set of rules : ' + JSON.stringify(firstArray) + ' \n');
//# sourceMappingURL=index.js.map