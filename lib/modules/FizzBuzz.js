'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * defaaultRules
 */

var defaultRules = {
	3: 'Fizz',
	5: 'Buzz'
};

/**
 * 
 */

var FizzBuzz = function () {

	/**
     * @param {object} rules - optional game rules object, schema : { number : string , ... } 
     */
	function FizzBuzz() {
		var _this = this;

		var rules = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : defaultRules;

		_classCallCheck(this, FizzBuzz);

		// sorting the RulesObject keys
		this.rules = Object.keys(rules).sort();

		// constructing the _insert method
		this._insert = function (index) {
			var insertion = '';
			_this.rules.forEach(function (element) {
				return index % element === 0 ? insertion = insertion + rules[element] : null;
			});
			insertion === '' ? insertion = index : null;
			return insertion;
		};

		this.array = null;
	}

	/**
     * generates and array based on the current rules
     * @param {int} firstInt 
     * @param {int} lastInt 
     */


	_createClass(FizzBuzz, [{
		key: 'generateArray',
		value: function generateArray() {
			var _this2 = this;

			var firstInt = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
			var lastInt = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 100;

			this.array = Array.from({ length: lastInt - firstInt + 1 }, function (value, index) {
				return _this2._insert(index + 1);
			});
		}

		/**
      * changes the rules on an existing instance of the class 
      * @param {object} rules - game rules object, schema : { number : string , ... } 
      */

	}, {
		key: 'changeRules',
		value: function changeRules(rules) {
			var _this3 = this;

			this.rules = Object.keys(rules).sort();
			this._insert = function (index) {
				var insertion = '';
				_this3.rules.forEach(function (element) {
					return index % element === 0 ? insertion = insertion + rules[element] : null;
				});
				insertion === '' ? insertion = index : null;
				return insertion;
			};
		}
	}]);

	return FizzBuzz;
}();

exports.default = FizzBuzz;
//# sourceMappingURL=FizzBuzz.js.map